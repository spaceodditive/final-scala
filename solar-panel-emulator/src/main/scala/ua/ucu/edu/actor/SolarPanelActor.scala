package ua.ucu.edu.actor

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import ua.ucu.edu.model.{ReadMeasurement}

import scala.language.postfixOps

/**
  * Keeps a list of device sensor actors, schedules sensor reads and pushes updates into sensor data topic
  */
class SolarPanelActor(
  val panelId: String
) extends Actor {
  implicit val system: ActorSystem = ActorSystem()

  var deviceToActorRef: Map[String, ActorRef] = Map()

  for (index <- 1 to 10) {
    val deviceId = s"${panelId}-device-${index}"
    val actorReference = system.actorOf(Props(classOf[SensorActor], deviceId, "sensor-type-1"), s"${deviceId}-manager")
    deviceToActorRef += (s"${panelId}-device-${index}" -> actorReference)
  }

  override def receive: Receive = {
    case ReadMeasurement => {
      deviceToActorRef foreach { case (key, value) => value ! ReadMeasurement }
    }
  }
}
