package ua.ucu.edu.actor

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import ua.ucu.edu.model.{Location, ReadMeasurement}

/**
  * This actor manages solar plant, holds a list of panels and knows about its location
  * todo - the main purpose right now to initialize panel actors
  */
class PlantManagerActor(
  plantName: String,
  location: Location
) extends Actor {
  implicit val system: ActorSystem = ActorSystem()
  var panelToActorRef: Map[String, ActorRef] = Map()

  for (index <- 1 to 10) {
    val panelId = s"${plantName}-panel-${index}"
    val actorReference = system.actorOf(Props(classOf[SolarPanelActor], panelId), s"${panelId}-manager")
    panelToActorRef += (s"${plantName}-panel-${index}" -> actorReference)
  }

  override def receive: Receive = {
    case ReadMeasurement => {
      panelToActorRef foreach { case (key, value) => value ! ReadMeasurement }
    }
  }
}
