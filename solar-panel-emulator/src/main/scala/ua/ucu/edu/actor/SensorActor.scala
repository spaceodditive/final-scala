package ua.ucu.edu.actor

import akka.actor.{Actor}
import ua.ucu.edu.device.{SensorApi, SensorGenerator}
import ua.ucu.edu.kafka.PlantDataProducer
import ua.ucu.edu.model.{ReadMeasurement, RespondMeasurement}

import scala.language.postfixOps

class SensorActor(
  val deviceId: String,
  sensorType: String
) extends Actor {

  val api: SensorApi = new SensorGenerator

  override def receive: Receive = {
    case ReadMeasurement => {
      val sensorValue = "%.3f".format(api.readCurrentValue())
      PlantDataProducer.pushData(new RespondMeasurement(deviceId, sensorType, sensorValue))
    }
  }
}
