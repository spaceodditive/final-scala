package ua.ucu.edu.model

// Coordinates of Lviv are defaults.
case class Location(longitude: String = "49.83826", latitude: String = "24.02324")
