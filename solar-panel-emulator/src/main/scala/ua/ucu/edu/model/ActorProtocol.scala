package ua.ucu.edu.model

case object ReadMeasurement

case class RespondMeasurement(deviceId: String, sensorType: String, value: String){
  override def toString(): String = s"deviceID: ${deviceId}, sensorType: ${sensorType}, value: ${value}"
}