package ua.ucu.edu

import scala.concurrent.duration
import akka.actor._
import ua.ucu.edu.actor.PlantManagerActor
import ua.ucu.edu.model.{Location, ReadMeasurement}

object Main extends App {
  import system.dispatcher

  import duration._
  implicit val system: ActorSystem = ActorSystem()

  var plantToActorRef: Map[String, ActorRef] = Map()

  for (index <- 1 to 20) {
    val actorReference = system.actorOf(Props(classOf[PlantManagerActor], s"plant-${index}", Location()), s"plant-${index}-manager")
    plantToActorRef += (s"plant-${index}" -> actorReference)

  }

  system.scheduler.schedule(0 seconds, 3 seconds, new Runnable {
    override def run(): Unit = {
      plantToActorRef foreach { case (key, value) => value ! ReadMeasurement }
    }
  })



}