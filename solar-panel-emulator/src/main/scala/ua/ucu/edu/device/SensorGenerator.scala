package ua.ucu.edu.device

class SensorGenerator extends SensorApi {
  override def readCurrentValue() : Float = {
    val r = scala.util.Random
    r.nextFloat()
  }
}
