package ua.ucu.edu.kafka

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.slf4j.{Logger, LoggerFactory}
import ua.ucu.edu.model.RespondMeasurement

// delete_me - for testing purposes
object PlantDataProducer {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  // This is just for testing purposes
  def pushData(measurement: RespondMeasurement): Unit = {
    val BrokerList: String = System.getenv(Config.KafkaBrokers)
    val Topic = "lut_sensor_data"

    val props = new Properties()
    props.put("bootstrap.servers", BrokerList)
    props.put("client.id", "solar-panel-1")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    logger.info("initializing producer")

    val producer = new KafkaProducer[String, String](props)

    val msg = measurement

    logger.info(s"[$Topic] $msg")
    val data = new ProducerRecord[String, String](Topic, msg.toString())
    producer.send(data)


    producer.close()
  }
}

object Config {
  val KafkaBrokers = "KAFKA_BROKERS"
}