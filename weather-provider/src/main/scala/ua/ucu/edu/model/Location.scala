package ua.ucu.edu.model

case class Location(longitude: String, latitude: String) {
  override def toString = s"${longitude},${latitude}"
}
