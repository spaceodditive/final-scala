package ua.ucu.edu

import ua.ucu.edu.kafka.WeatherDataProducer
import akka.actor.ActorSystem
import org.slf4j.LoggerFactory
import ua.ucu.edu.model.Location

import scala.concurrent.duration
import scala.language.postfixOps
import scala.io.Source

object Main extends App {

  val logger = LoggerFactory.getLogger(getClass)

  logger.info("======== Weather Provider App Init ========")

  val system = ActorSystem()
  import system.dispatcher

  import duration._

  system.scheduler.schedule(0 seconds, 120 seconds, new Runnable {
    override def run(): Unit = {
      logger.info("Dark Sky weather request")
      // Powered by Dark Sky https://darksky.net/poweredby/
      val baseUrl = "https://api.darksky.net/forecast/"
      val secret = "cac826cc19f6c7215277c162e41b5833"
      val location = new Location("49.83826", "24.02324")
      val weather = scala.io.Source.fromURL(baseUrl + secret + "/" + location).mkString
      logger.info(weather)
      WeatherDataProducer.pushData(weather)
      // todo - ask weather api and send data to kafka topic - recommended format is json - or you can come up with simpler string-based protocol
    }
  })
}
